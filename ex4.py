# -- coding: utf-8 --

"""
Created on Fri Mar 13 15:10:17 2020

@author: edouard.roger
"""
from utils.operations import SimpleCalculator

CALC = SimpleCalculator(10, 2)
SOMME = CALC.sum()
SOUSTRACTION = CALC.subtract()
MULTIPLICATION = CALC.multiply()
DIVISION = CALC.divide()

print(SOMME)
print(SOUSTRACTION)
print(MULTIPLICATION)
print(DIVISION)
